FROM devportalpro/nuxt3:1.0.0
ADD . /opt/app
RUN npm update \
    && rm app.vue \
    && npm run build \
    && rm -rf /var/lib/apt/lists/*

CMD [ "npm", "run", "start" ]