import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    head: {
        title: 'my title',
        meta: [
            { charset: 'utf-8' }
        ]

    },

    css: [
        '~/assets/root.css',
        '~/assets/fonts.css',
        '~/assets/main.css',
        '~/assets/mq/320-900.css',
        '~/assets/menu.css',
        '~/assets/forms.css',


    ],
    app:{
        buildAssetsDir:"/startup/",
    },

    buildAssetsDir:"/startup/",
})


